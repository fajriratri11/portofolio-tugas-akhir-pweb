
 const pagesWithPopup = ["index", "contact", "about"];

 window.addEventListener('DOMContentLoaded', function() {
     const currentPage = getCurrentPageName();
     showPopupIfApplicable(currentPage);
 });

 function getCurrentPageName() {
     const currentPath = window.location.pathname;
     const currentPageName = currentPath.split('/').pop().split('.')[0];
     return currentPageName.toLowerCase(); 
 }

 function showPopupIfApplicable(page) {
     if (pagesWithPopup.includes(page)) {
         showWelcomePopup();
     }
 }

 function showWelcomePopup() {
     alert("SELAMAT DATANG");
 }