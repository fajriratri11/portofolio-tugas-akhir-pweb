<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Contact Form</title>
    <link rel="stylesheet" href="program.css">
</head>
<body>
    <!--navigation bar-->
    <header>
        <nav>
            <ul class="ul-navbar">
                <li class="li-navbar">
                    <a href="index.php" class="a-navbar">Home</a>
                </li>
                <li class="li-navbar">
                    <a href="contact.html" class="a-navbar">Contact</a>
                </li>
                <li class="li-navbar">
                    <a href="about.html" class="a-navbar">About</a>
                </li>
            </ul>
        </nav>
    </header>
    <!--end navigation bar-->
    <section>
        <aside>
            <img src="https://purepng.com/public/uploads/large/purepng.com-man-face-herofaceshumansfrontalhuman-identityman-1421526885661w7dlt.png">
        </aside>
        <main>
            <article>
                <h1 class="h1-artikel">Hello I'm Fajri</h1>
                <br>
                <br>
                <p class="p-artikel">Selamat datang di web portofolio pertamaku. <br>
                    Perkenalkan nama saya Mohammad Fajri Ratri,<br>
                    saya sekarang berumur 19 tahun dan tahun ini genap 20 tahun.<br>
                </p>

                <!-- Contact Form -->
                <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
                    <label for="name">Name:</label>
                    <input type="text" id="name" name="name" required><br>

                    <label for="email">Email:</label>
                    <input type="email" id="email" name="email" required><br>

                    <label for="message">Message:</label><br>
                    <textarea id="message" name="message" rows="4" cols="50" required></textarea><br>

                    <input type="submit" name="submit" value="Submit">
                </form>

                <?php
                // Check if the form is submitted
                if (isset($_POST["submit"])) {
                    // Get the form data
                    $name = $_POST["name"];
                    $email = $_POST["email"];
                    $message = $_POST["message"];

                    // Store the form data in an array
                    $formSubmissions = array();
                    $formSubmissions[] = array(
                        "name" => $name,
                        "email" => $email,
                        "message" => $message
                    );

                    // Display all form submissions using a loop
                    echo "<h2>Form Submissions:</h2>";
                    foreach ($formSubmissions as $submission) {
                        echo "<p>Name: " . htmlspecialchars($submission['name']) . "</p>";
                        echo "<p>Email: " . htmlspecialchars($submission['email']) . "</p>";
                        echo "<p>Message: " . htmlspecialchars($submission['message']) . "</p>";
                        echo "<hr>";
                    }
                }
                ?>
                
            </article>
        </main>
    </section>
    <footer>
        <p class="p-footer"> Fajri Portfolio</p>
    </footer>
    <script src="portofolio.js"></script>
</body>
</html>
